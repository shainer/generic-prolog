reverseAcc([], A, A).
reverseAcc([H|T], A, R) :- reverseAcc(T, [H|A], R).
rev(L, R) :- reverseAcc(L, [], R).

accLen([], A, A).
accLen([_|T], A, L) :- Anew is A+1, accLen(T, Anew, L).

toptail([H | T], OutList) :- accLen([H | T], 0, Len), Len > 1,
			      rev(T, [_ | Tnew]),
			      rev(Tnew, OutList).
