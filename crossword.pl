word(astante,  a,s,t,a,n,t,e). 
word(astoria,  a,s,t,o,r,i,a). 
word(baratto,  b,a,r,a,t,t,o). 
word(cobalto,  c,o,b,a,l,t,o). 
word(pistola,  p,i,s,t,o,l,a). 
word(statale,  s,t,a,t,a,l,e). 

crossword(P1, P2, P3, P4, P5, P6):-
  word(P1, L21, L22, L23, L24, L25, L26, L27),
  word(P2, L41, L42, L43, L44, L45, L46, L47),
  word(P3, L61, L62, L63, L64, L65, L66, L67),
  word(P4, L12, L22, L32, L42, L52, L62, L72),
  word(P5, L14, L24, L34, L44, L54, L64, L74),
  word(P6, L16, L26, L36, L46, L56, L66, L76).