byCar(auckland,hamilton). 
byCar(hamilton,raglan). 
byCar(valmont,saarbruecken). 
byCar(valmont,metz). 

byTrain(metz,frankfurt). 
byTrain(saarbruecken,frankfurt). 
byTrain(metz,paris). 
byTrain(saarbruecken,paris). 

byPlane(frankfurt,bangkok). 
byPlane(frankfurt,singapore). 
byPlane(paris,losAngeles). 
byPlane(bangkok,auckland). 
byPlane(singapore,auckland). 
byPlane(losAngeles,auckland).

bySomething(X, Y) :- byCar(X, Y); byTrain(X, Y); byPlane(X, Y).
go(X, Y) :- bySomething(X, Y).
go(X, Y, go(_W, _Z)) :- bySomething(X, Y).
travel(X, Y, go(X, Y)).
travel(X, Y, go(X, W, Z)) :- go(X, W), travel(W, Y, Z).