append([], L, L).
append([H|T], L2, [H | L3]) :- append(T, L2, L3).

start(Z) :- np(X), vp(Y), append(X, Y, Z).
np(Z) :- det(X), n(Y), append(X, Y, Z).
vp(Z) :- v(X), np(Y), append(X, Y, Z).
vp(Z) :- v(Z).

det([the]).
det([a]).

n([woman]).
n([man]).

v([shoots]).