sumElem([X, Y, Z]) :- Z is X + Y, !.
sumElem([X, Y, Z | Tail]) :- Z is X + Y, !, sumElem([Y, Z | Tail]). 
