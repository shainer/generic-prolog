greater(succ(0), 0).
greater(succ(X), succ(Y)) :- greater(X, Y).