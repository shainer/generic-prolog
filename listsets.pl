mysubset([], _).
mysubset([H1 | T1], L2) :- member(H1, L2), mysubset(T1, L2). 

mysuperset(_, []).
mysuperset(L1, [H2 | T2) :- member(H2, L1), mysuperset(L1, T2).