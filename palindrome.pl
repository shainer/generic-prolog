accRev([], A, A).
accRev([H | T], A, R) :- accRev(T, [H | A], R). 
rev(L, R) :- accRev(L, [], R).

palindrome(L) :- rev(L, L).
