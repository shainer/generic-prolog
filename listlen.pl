len([], 0).
len([_ | T], L) :- len(T, X), L is X+1.