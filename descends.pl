descend(X,Y)  :-  child(X,Y). 
descend(X,Y)  :-  descend(X,Z), descend(Z,Y).

child(anne, bridget).
child(bridget, donna).
child(donna, caroline).
child(caroline, lisa).