btree(void).
btree(tree(_Element, Left, Right)) :- btree(Left), btree(Right).

%bf(void, []).
%bf(tree(Element, Left, Right), [Element | T]) :- append(L, Left, L), append(L, Right, L), bf(Left, T), bf(Right, T).

bf([], []).
bf([void | Rest], Ls) :- bf(Rest, Ls).
bf([tree(I, Dx, Sx) | Rest], [I | Ls]) :- append(Rest, [Dx, Sx], Nodes), bf(Nodes, Ls).

% bf([ tree(a, tree(b, void, void), tree(c, tree(d, void, void), void)) ], L).

% 1° iterazione: I = a, Dx = tree(b, void, void), Sx = tree(c, tree(d, void, void), void), Rest=[]
[a | Ls]

Rest + [Dx, Sx] = [Dx, Sx] = Nodes.

% 2° iterazione: tree(I, Dx, Sx) = tree(b, void, void), quindi I = b, Dx = Sx = void, Rest = [tree(c, tree(d, void, void), void)]
[b | Ls]
Rest + [void, void] = [tree(c, tree(d, void, void), void), void, void] = Nodes.

% 3° iterazione: tree(I, Dx, Sx) = tree(c, tree(d, void, void), void), Rest = [void, void]
[c | Ls]

Rest + [tree(d, void, void), void] = [tree(d, void, void), void, void, void] = Nodes.

% La coda inizialmente contiene la radice. Ad ogni iterazione si prende il primo nodo della coda e ci si mettono i figli dentro.
% L\'elemento correntemente estratto dalla coda viene messo nella lista finale.