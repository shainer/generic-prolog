scalarMult(I, [], []).
scalarMult(I, [H | T], [HR | TR]) :- HR is H * I, scalarMult(I, T, TR).