tran(ett, uno).
tran(tva, due).
tran(tre, tre).
tran(fyra, quattro).
tran(fem, cinque).
tran(sex, sei).
tran(sju, sette).
tran(atta, otto).
tran(nio, nove).
tran(tio, dieci).

listtran([], []).
listtran([Hs | Ts], [Hi | Ti]) :- tran(Hs, Hi), listtran(Ts, Ti).