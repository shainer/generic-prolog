reverseAcc([], A, A).
reverseAcc([H|T], A, R) :- reverseAcc(T, [H|A], R).
rev(L, R) :- reverseAcc(L, [], R).

final(X, List) :- rev(List, [X | _]).
