teacher(nn,uniroma1).
university(a, b).

professor(dn,uniroma1).
professor(mv,cmu).

professor(X, Y) :- university(X, Y).
adjunct(X) :- teacher(X,Y), \+professor(X,Y).
