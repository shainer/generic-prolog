accMax([], A, A).
accMax([H | T], A, M) :- H > A, accMax(T, H, M).
accMax([H | T], A, M) :- H =< A, accMax(T, A, M).

max(List, Max) :- List = [H|_], accMax(List, H, Max).