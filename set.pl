member_p(X, [X|_]).
member_p(X, [_|T]) :- member_p(X, T). 

set([], []).
set([H1 | T1], [H2 | T2]) :- member_p(H1, [H2 | T2]).