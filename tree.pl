leaf(_X).
tree(leaf(_X), leaf(_Y)).
tree(tree(_X), leaf(_Y)).
tree(leaf(_X), tree(_Y)).
tree(tree(_X), tree(_Y)).
 
swap(leaf(X), leaf(X)).
swap(tree(X1, Y1), tree(Y2, X2)) :- swap(X1, X2), swap(Y1, Y2).