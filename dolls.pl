directlyContains(katarina, olga).
directlyContains(olga, natasha).
directlyContains(natasha, irina).

contains(X, Y) :- directlyContains(X, Y).
contains(X, Y) :- directlyContains(X, Z), contains(Z, Y).