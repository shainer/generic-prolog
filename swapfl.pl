equal([], []).
equal([H3 | T3], [H3 | T4]) :- equal(T3, T4).

reverseAcc([], A, A).
reverseAcc([H|T], A, R) :- reverseAcc(T, [H|A], R).
rev(L, R) :- reverseAcc(L, [], R).

accLen([], A, A).
accLen([_|T], A, L) :- Anew is A+1, accLen(T, Anew, L).

toptail([H | T], OutList) :- accLen([H | T], 0, Len), Len > 1,
			      rev(T, [_ | Tnew]),
			      rev(Tnew, OutList).

swapfl([], []).
swapfl([H1 | T1], [H2 | T2]) :- last([H1 | T1], H2),
				 last([H2 | T2], H1),
				 toptail([H1 | T1], FirstListNew),
				 toptail([H2 | T2], SecondListNew),
				 equal(FirstListNew, SecondListNew).
