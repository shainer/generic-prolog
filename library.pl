same_author([], _, []).
same_author([book(T, Author, Y) | Library], Author, [book(T, Author, Y) | Result]) :- same_author(Library, Author, Result).
same_author([_ | Library], Author, Result) :- same_author(Library, Author, Result). 
